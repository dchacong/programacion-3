package com.umg.reactivo;

import rx.Observable;
import rx.observables.MathObservable;
import rx.functions.Func2;

public class Serie2 {
    public static void main(String[] args) {
    Integer[] numbers = {2,5,6,8,10,35,2,10};
    Observable miObservable_control1 = Observable.from(numbers);
    MathObservable.averageInteger(miObservable_control1)
            .subscribe((promedio) -> {
                System.out.println("1) PROMEDIO:" + promedio);
            });


    Observable miObservable_control2 = Observable.from(numbers).filter((element) -> element >= 10);
    System.out.println();
    System.out.println("2) Mayore que 10");
    miObservable_control2.subscribe((elemet) -> System.out.println("*- Mayores que 10: " +elemet));

    Observable miObservable_control3 = Observable.from(numbers).reduce(
            new Func2<Integer, Integer, Integer>() {
                @Override
                public Integer call(Integer acumulador, Integer actual) {
                    return acumulador + actual;
                }
            });
    System.out.println();
    miObservable_control3.subscribe((element) -> System.out.println("3) Sumatoria: " + element));

    }
}
