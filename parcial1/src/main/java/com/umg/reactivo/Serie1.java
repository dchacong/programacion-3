package com.umg.reactivo;

import rx.Observable;
import dto.Carros;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Serie1 {
    public static void main(String[] args) {
        AtomicInteger sumatoria = new AtomicInteger();
        AtomicInteger max = new AtomicInteger();
        AtomicInteger sumatoriaConA = new AtomicInteger();


        List<Carros> carros = new ArrayList<>();
        carros.add(new Carros("vehiculo_simple", 300));
        carros.add(new Carros("vehiculo_simple_auto", 300));
        carros.add(new Carros("vehiculo_doble_traccion", 200));
        carros.add(new Carros("vehiculo_alta_gama", 800));
        carros.add(new Carros("motocicleta", 230));

        Observable miObservable_carros =
                Observable
                        .from(carros.toArray())
                        .map((element) -> {
                            Carros ele = (Carros) element;
                            if(ele.getNombre().contains("a")){
                                sumatoriaConA.addAndGet(ele.getValor());
                            } else {
                                System.out.println("Elemento sin A: " + ele.getNombre());
                                System.out.println("");
                            }
                            return ele.getValor();
                        });

        miObservable_carros.subscribe(
                (element)-> {
                    sumatoria.addAndGet((int) element);
                    if (max.intValue() < (int) element){
                        max.set((int) element);
                    }

                },
                (element)->{},
                () -> {} );
        System.out.println("1)\tsumatorio:" + sumatoria.get());
        System.out.println("2)\tMaximo:" + max.get());
        System.out.println("3)\tSumatorio de vehiculos con A:" + sumatoriaConA.get());

    }
}
