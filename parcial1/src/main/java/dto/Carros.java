package dto;

public class Carros {
    private String nombre;
    private int valor;
    public Carros(String nombre, int valor){
        this.nombre = nombre;
        this.valor = valor;
    }

    public void setNombre(String nombre){this.nombre = nombre;}
    public String getNombre(){return nombre;}
    public void setValor(int valor){this.valor = valor;}
    public int getValor(){return valor;}
}
